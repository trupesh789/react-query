import React from "react";
import { useHistory } from "react-router-dom";
import { Button, Heading } from "@chakra-ui/react";

const NavBar = () => {
  const history = useHistory();
  return (
    <div>
      <Heading my={5} as="h5" size="lg">
        NavBar
      </Heading>
      <Button
        colorScheme="teal"
        size="md"
        variant="outline"
        onClick={() => history.push("/react-query")}
      >
        React Query
      </Button>
      <Button
        colorScheme="teal"
        size="md"
        ml={3}
        onClick={() => history.push("/react-hook-form")}
      >
        React Hook Form
      </Button>{" "}
      <Button
        colorScheme="teal"
        size="md"
        ml={3}
        variant="outline"
        onClick={() => history.push("/react-hook-form-api")}
      >
        React Hook Form with Api data
      </Button>
    </div>
  );
};

export default NavBar;
