import React from "react";
import {
  Button,
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  Radio,
  RadioGroup,
  Stack,
} from "@chakra-ui/react";

const MultiFieldArray = ({ register, fields, remove, errors }) => {
  console.log(errors);
  return fields.map((item, index) => (
    <>
      {/* {console.log(index)} */}
      <FormControl mt={3} mb={4}>
        <FormLabel>First name</FormLabel>
        <Input
          key={item.id}
          type="text"
          {...register(`FieldArray.${index}.FMname`, {
            required: true,
            minLength: 4,
          })}
          placeholder="Enter Fname"
          defaultValue={item.FMname}
        />
        {errors?.FieldArray?.[index]?.FMname && (
          <FormHelperText color="red">Fname is required </FormHelperText>
        )}
      </FormControl>

      <FormControl mb={5}>
        <FormLabel>Last name</FormLabel>
        <Input
          key={item.id}
          type="text"
          {...register(`FieldArray.${index}.LMname`, {
            required: true,
            minLength: 4,
          })}
          placeholder="Enter Lname"
          defaultValue={item.LMname}
        />
        {errors?.FieldArray?.[index]?.LMname && (
          <FormHelperText color="red">Lname is required </FormHelperText>
        )}
      </FormControl>
      <FormControl mb={5}>
        <RadioGroup>
          <Stack direction="row">
            <Radio
              colorScheme="red"
              {...register(`FieldArray.${index}.Gender`, {
                required: true,
              })}
              value="male"
            >
              Male
            </Radio>
            <Radio
              {...register(`FieldArray.${index}.Gender`, {
                required: true,
              })}
              value="female"
            >
              Female
            </Radio>
            <Radio
              {...register(`FieldArray.${index}.Gender`, {
                required: true,
              })}
              value="other"
            >
              Other
            </Radio>
          </Stack>
        </RadioGroup>
        {errors?.FieldArray?.[index]?.Gender && (
          <FormHelperText color="red">This field is req</FormHelperText>
        )}
      </FormControl>

      <Button
        colorScheme="red"
        variant="outline"
        mr={5}
        type="button"
        onClick={() => remove(index)}
        disabled={index === 0}
      >
        Remove
      </Button>
    </>
  ));
};

export default MultiFieldArray;
