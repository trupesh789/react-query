/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { Box, Button, FormControl, FormLabel, Input } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { useQuery } from "react-query";

const ReactHookFormApi = () => {
  const { handleSubmit, register, reset } = useForm();

  // const { isLoading, error, data } = useQuery("userData", () =>
  //   fetch("https://jsonplaceholder.typicode.com/users/3").then((res) =>
  //     res.json()
  //   ),
  // );

  const { isLoading, error, data } = useQuery({
    queryKey: "userData",
    queryFn: () =>
      fetch("https://jsonplaceholder.typicode.com/users/10").then((res) =>
        res.json()
      ),
    onSuccess: (data) => reset(data),
  });

  // console.log(data, `Error is ${error}`);

  // useEffect(() => {
  //   !isLoading && !error && data && reset(data);
  //   console.log("component did mount");
  // }, []);

  // const filearray = ["tru", "cha", "sur"];

  // if (filearray.includes("tru")) {
  //   console.log("yes it's match");
  // } else {
  //   console.log("it's not matchs");
  // }

  const onSubmit = (data) => {
    console.log(data.userProfile);
  };

  console.log("pr branch");

  console.log("It's come from master");

  return (
    <Box mt={3} w="400px">
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl mt={3} mb={4}>
          <FormLabel>First name</FormLabel>
          <Input {...register("name")} type="text" placeholder="Enter Fname" />
        </FormControl>

        <FormControl mt={3} mb={4}>
          <FormLabel>Email</FormLabel>
          <Input {...register("email")} type="text" placeholder="Enter Fname" />
        </FormControl>

        <FormControl mt={3} mb={4}>
          <FormLabel>City</FormLabel>
          <Input
            {...register("address.city")}
            type="text"
            placeholder="Enter Fname"
          />
        </FormControl>

        <FormControl mt={3} mb={4}>
          <FormLabel>Phone</FormLabel>
          <Input {...register("phone")} type="text" placeholder="Enter Fname" />
        </FormControl>

        <FormControl mt={3} mb={4}>
          <FormLabel>User Profile</FormLabel>
          <Input
            {...register("userProfile")}
            type="file"
            placeholder="Enter Fname"
          />
        </FormControl>

        <Button colorScheme="teal" type="submit">
          Submit
        </Button>
      </form>
    </Box>
  );
};

export default ReactHookFormApi;
