/* eslint-disable react-hooks/exhaustive-deps */
import { Box, Button, Flex } from "@chakra-ui/react";
import React, { useEffect } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import MultiFieldArray from "./field-array";

const ReactHookForm = () => {
  const {
    handleSubmit,
    formState: { errors },
    register,
    control,
    getValues,
  } = useForm({
    // defaultValues: {
    //   FMname: "",
    //   LMname: "",
    // },
  });

  useEffect(() => {
    append({ FMname: "", LMname: "" });
  }, []);

  const { fields, append, remove } = useFieldArray({
    control,
    name: "FieldArray",
  });
  const onSubmit = (data) => console.log(data);

  const asdd = () => {
    const asd = getValues(`FieldArray.${[0]}.FMname`);
    console.log(asd);
  };

  return (
    <Box mt={3} w="400px">
      <form onSubmit={handleSubmit(onSubmit)}>
        <MultiFieldArray
          register={register}
          fields={fields}
          remove={remove}
          errors={errors}
        />
        <Button
          colorScheme="teal"
          variant="outline"
          mr={5}
          type="button"
          onClick={() => append({ FMname: "", LMname: "" })}
        >
          Add
        </Button>
        <Button
          colorScheme="teal"
          variant="outline"
          mr={5}
          type="button"
          // onClick={() => console.log(getValues(`FieldArray`))}
          onClick={() => asdd()}
        >
          gettt
        </Button>
        <Flex mt={4}>
          <Button colorScheme="teal" type="submit">
            Submit
          </Button>
        </Flex>
      </form>
    </Box>
  );
};

export default ReactHookForm;
