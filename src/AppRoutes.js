import ReactQuery from "./react-query/react-query";
import ReactHookForm from "./react-hook-form/react-hook-form";
import NavBar from "./nav-bar";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import ReactHookFormApi from "./react-hook-form/react-hook-form-api";

export default function AppRoutes() {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route exact path="/react-query">
          <ReactQuery />
        </Route>
        <Route exact path="/react-hook-form">
          <ReactHookForm />
        </Route>
        <Route exact path="/react-hook-form-api">
          <ReactHookFormApi />
        </Route>

        <Redirect to="/react-query" />
      </Switch>
    </Router>
  );
}
