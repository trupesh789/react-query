import { useQuery } from "react-query";
import { Text, Stack, Spinner } from "@chakra-ui/react";

const ReactQuery = () => {
  const { isLoading, error, data } = useQuery("reposData", () =>
    fetch("https://api.covid19india.org/data.json").then((res) => res.json())
  );

  if (isLoading)
    return (
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="blue.500"
        size="xl"
      />
    );

  if (error) return "An error has occurred: " + error.message;

  console.log(data);
  return (
    <Stack spacing={2}>
      <Text fontSize="2xl" my={3}>
        See consoles
      </Text>
      {data.statewise.map((as) => {
        return (
          <>
            <h3>{`State: ${as.state}`}</h3>
            <h3>{`Active case" ${as.active}`}</h3>
          </>
        );
      })}
    </Stack>
  );
};

export default ReactQuery;
